﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerScript : BaseCharacter
{
    public LayerMask enemyLayers;
    public HealthBar healthBar;
    private bool IsDefence;
    private bool LockCursor = true;
    private UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter ThPersonComponent;
    

    public float Regeneration = 2f;
    public float RegenerationTime = 4f;
    private float DelayRegenerationTime;
    public float attackPerSecond = 5f;
    private float attackCooldown = 0f;

        
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DangerousObject")
        {
            takeDamage(15);
        }
    }

    protected override void SetCurrentHealth(float NewHealth){
        currentHealth = NewHealth;
        healthBar.SetHealth(currentHealth);
    }

    public override void takeDamage(float damage)
    {
        if(IsDefence){
            damage /= DefenceSkills;
        }
        currentHealth -= damage;
        if(damage > 10f){
            animator.SetTrigger("GetDamage");
        }

        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0)
        {
            die();
        }
    }

    protected override void die()
    {
        animator.SetBool("Dead", true);
        Destroy(GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>());

        Debug.Log("Вы умерли. Плохая концовка!");

        /* Здесь вызов концовки */
    }


    public override void Attack()
    {
        if(attackCooldown <= 0f)
        {
            attackCooldown = 1f / attackPerSecond;
            // атакует всех, оказавшихся в радиусе круга с радиусом attackRange
            Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);

            foreach (Collider enemy in hitEnemies)
            {
                enemy.GetComponent<Enemy>().takeDamage(attackDamage);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null) {
            Debug.Log("No attack Point");
            return;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        animator = GetComponent<Animator>();
        ThPersonComponent = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if(currentHealth < maxHealth){
            DelayRegenerationTime += Time.deltaTime;
            if(DelayRegenerationTime >= RegenerationTime){
                SetCurrentHealth(currentHealth + Regeneration);
                DelayRegenerationTime = 0f;
            }
        }

        attackCooldown -= Time.deltaTime;
     
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            animator.SetTrigger("Punching");
            Attack();
        }

        if(Input.GetKey(KeyCode.F))
        {
            animator.SetBool("Blocking", true);
            IsDefence = true;
        }
        else {
            animator.SetBool("Blocking", false);
            IsDefence = false;
        }
        // хороший велосипед
        if(Input.GetKeyDown(KeyCode.H)){      
            LockCursor = !LockCursor;
            Cursor.visible = !LockCursor;
            Cursor.lockState = LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
        }
    }

    void FixedUpdate(){
        if(Input.GetKey(KeyCode.LeftShift)){
            ThPersonComponent.SpeedUp(true);
        }
        else{
            ThPersonComponent.SpeedUp(false);
        }
    }
}
