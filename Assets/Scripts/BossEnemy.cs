﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy: Enemy
{
    public float HPToFuryMode = 40f;
    public float AddHP = 40f;
    private float BoostAttackSpeed = 2f;

    private bool FlagFuryMode = false;

    private float TimeNotificationOn;
    public GameObject NotificationForFuryMode;
    private float DurationOfNotification = 5f;

    private void EnableFuryMode()
    {
        currentHealth = currentHealth < 0 ? 0 : currentHealth;
        SetCurrentHealth(currentHealth + AddHP);
        attackDamage *= 2;
        DefenceSkills = 4;
        attackSpeed *= BoostAttackSpeed;
        NotificationForFuryMode.SetActive(true);
        TimeNotificationOn = Time.time;
    }

    protected override void die() 
    {
        base.die();

        Debug.Log("Босс убит. Хорошая концовка!");
        /* a здесь хорошая концовка */
    }
    
    public override void takeDamage(float damage) 
    {
        damage /= DefenceSkills;
        currentHealth -= damage;

        animator.SetTrigger("GetDamage");

        // сбивать атаку при уроне
        attackCooldown = 1f;
        
        if (currentHealth <= 0 && FlagFuryMode){
            die();
        }
        else if(!FlagFuryMode && currentHealth <= HPToFuryMode) {
            EnableFuryMode();
            FlagFuryMode = true;
        }
    }

    protected override void Update()
    {
        base.Update();

        if(Time.time >= TimeNotificationOn + DurationOfNotification){
            NotificationForFuryMode.SetActive(false);
        }
    }
}
