﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// для автоматического добавления компонентов в gameobject
// теперь надо добавить к объекту только скрипт enemy и enemyAI
[RequireComponent(typeof(Animator))]

public class Enemy : BaseCharacter
{
    
    public LayerMask playerLayer;
    public float attackSpeed = 1f;
    public float attackCooldown = 0f;
    public bool IsRest = true;

    public override void Attack()
    {
        if(attackCooldown <= 0f)
        {
            attackCooldown = 1f / attackSpeed;

            Collider[] hitPlayers = Physics.OverlapSphere(attackPoint.position, attackRange, playerLayer);
            
            animator.SetTrigger("Punching");
            foreach (Collider player in hitPlayers)
            {
                player.GetComponent<PlayerScript>().takeDamage(attackDamage);
            }
        }
    }

    public override void takeDamage(float damage)
    {
        damage /= DefenceSkills;
        currentHealth -= damage;
        animator.SetTrigger("GetDamage");

        // сбивать атаку при уроне
        attackCooldown = 1f;

        if (currentHealth <= 0)
        {
            die();
        }
    }

    protected override void die()
    {
        Destroy(GetComponent<UnityStandardAssets.Characters.ThirdPerson.EnemyAI>());
        animator.SetBool("Dead", true);
    }


    protected void Start()
    {
        currentHealth = maxHealth;
        animator = this.GetComponent<Animator>();

    }

    protected virtual void Update()
    {
        attackCooldown -= Time.deltaTime;
        if (IsRest){
            animator.SetBool("IsRest", true);
        }
        else {
            animator.SetBool("IsRest", false);
        }
    }
    protected void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}