﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject enemyOnBridge;
    public int nEnemyOnBridge;
    private Vector3 SpawnLocationOnBridge;
    public float CoefAllocation = 2f;
    private static bool  IsTriggerOnBridge = false;

    public static void SetIsTriggerOnBridge(bool IsTrigger){
        IsTriggerOnBridge = IsTrigger;
    }

    private void Start(){
        enemyOnBridge.SetActive(false);
        SpawnLocationOnBridge = enemyOnBridge.transform.position;
    }
    
    private void Update(){
        if(IsTriggerOnBridge){
            IsTriggerOnBridge = false;
            enemyOnBridge.SetActive(true);
            for(int i = 0; i < nEnemyOnBridge - 1; i++)
            {
                Instantiate(enemyOnBridge, new Vector3(SpawnLocationOnBridge.x + i*CoefAllocation, 
                SpawnLocationOnBridge.y + 1f, SpawnLocationOnBridge.z + i*CoefAllocation), Quaternion.identity);
            }
        }
    }
}
