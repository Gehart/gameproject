﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    public GameObject NPC;
    private Vector3 PlayerPos;
    private Vector3 NPCPos;
    public float remainingDistance = 3f;
    public GameObject FirstDialogueMessage;
    public GameObject NotificationMessage;
    public bool FaceToPlayer = true;
    private bool IsClose;
    private bool IsAlreadyRead = false;

    void Start()
    {
    }

    void Update()
    {
        PlayerPos = PlayerManager.instance.player.transform.position;
        NPCPos = NPC.transform.position;
        if(Vector3.Distance(PlayerPos, NPCPos) <= remainingDistance) {
            IsClose = true;
        }
        else {
            IsClose = false;
        }
        if(IsClose){
            if(FaceToPlayer){
                FaceTarget(PlayerPos);
                NPC.GetComponent<Animator>().SetBool("IsSpeaking", true);
            }
            if(!IsAlreadyRead){
                NotificationMessage.SetActive(true);
            }

            if(Input.GetKeyDown(KeyCode.E)){
                FirstDialogueMessage.SetActive(true);
                NotificationMessage.SetActive(false);
                IsAlreadyRead = true;
                NPC.GetComponent<Animator>().SetBool("IsSpeaking", true);
                
            }
        }
        else if(!IsClose){
            NotificationMessage.SetActive(false);
            IsAlreadyRead = false;
        }
    }
    
    void FaceTarget(Vector3 PlayerPos)
        {
            Vector3 direction = (PlayerPos - NPC.transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, lookRotation, Time.deltaTime * 5f);
        }
}
