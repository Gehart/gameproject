﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pause_menu : MonoBehaviour
{
    private static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause_menu();
                
            }
        }
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        // Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;

    }
    void Pause_menu()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f; //остановка времени
        GameIsPaused = true;
        // Cursor.lockState = CursorLockMode.None;
        // Cursor.visible = true;
    }
    public void LoadMenu()
    {
        Debug.Log("Load");
        Time.timeScale = 1f;
        SceneManager.LoadScene("меню");
    }
      public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
