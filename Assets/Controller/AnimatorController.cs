﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    Animator animator;

    float vertical;
    float horizontal;

    // Start is called before the first frame update
    void Start()
    {
        animator=GetComponent<Animator>();    
    }

    // Update is called once per frame
    void Update()
    {
        
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");

        if (vertical == 0){
            animator.SetBool ("Run",false);
            animator.SetBool ("RunBack",false);
            animator.SetBool ("RunLeft",false);
            animator.SetBool ("RunRight",false);
        }
    
        if (vertical >= 0.1f){
            animator.SetBool ("Run",true);
        }
        if (vertical <= -0.1f){
            animator.SetBool ("RunBack",true);
        }
        if (horizontal <= -0.1f){
            animator.SetBool ("RunLeft",true);
        }   
        if (horizontal >= 0.1f){
            animator.SetBool ("RunRight",true);
        }   
    }
}
